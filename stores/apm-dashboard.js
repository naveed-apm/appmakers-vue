import {Resource} from "../../appmakers-vue/services/http-resource-service";

const state = {
    config: [],
    loading: false,
    error: '',
    url: '',
};

const getters = {
    loading: state => state.loading,
    config: state => state.config,
    error: state => state.error,
    url: state => state.url,
};

const mutations = {
    setUrl(state, value) {
        state.url = value;
    },
    setLoading(state, value) {
        state.loading = value;
    },
    setConfig(state, value) {
        state.config = value;
    },
    setError(state, value) {
        state.error = value;
    },
};

const actions = {
    init({commit, dispatch}, config) {
        commit('setUrl', config.url);
    },
    load({commit, dispatch}) {
        const req = new Resource(state.url);
        commit('setLoading', true);
        req.send({
            onSuccess: (res) => {
                commit('setConfig', res.data);
                commit('setLoading', false);
            },
            onError: (res) => {
                commit('setError', res.data);
                commit('setLoading', false);
            }
        });
    },
};

export const dashboardStore = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};
