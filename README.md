appmakers-vue has a separate repo on bitbucket now.


### Installation

When you want to use it in other projects, clone the repo anywhere in the project. Or, ask for a copy from Naveed.

### Dependencies:

    npm i axios axios-cache-adapter bootstrap-vue lodash moment vuex vue-select vue-grid-layout vue-switches
    npm i node-sass sass-loader --save-dev

### Usage

In your `main.js`:

    import store from './store';
    import {AppMakersVue} from "./appmakers-vue";
    
    ...
    
    Vue.use(AppMakersVue, {store});

### How to get update code

    cd appmakers-vue
    git pull origin master

### How to add more code to the package

every time you make a change, please git commit and push the code, so it can be used in other projects.
