import {ComponentRegistrar} from "./component-registrar";
import {DirectiveRegistrar} from "./directive-registrar";
import {FilterRegistrar} from "./filter-registrar";
import {StoreRegistrar} from "./store-registrar";
import './utilities/apmFunctions';
import './styles/appmakers.scss';

export const AppMakersVue = {
    install(Vue, options) {
        ComponentRegistrar.register(Vue);
        DirectiveRegistrar.register(Vue);
        FilterRegistrar.register(Vue);

        if (!options || !options.store) {
            throw new Error("Please provide vuex store when registering AppMakersVue.");
        }
        StoreRegistrar.register(options.store);
    }
};
